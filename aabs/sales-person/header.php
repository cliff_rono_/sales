
<?php
ob_start();
session_start();
require_once "../config/pik-dbase-config.php";
require_once "../config/pik-functions.php";
require_once "php_functions/pik-functions.php";
//
//if(isset($_SESSION['super_agent_id'])){
//    $super_agent_id = $_SESSION['super_agent_id'];
//    $agent_number = $_SESSION['super_agent_number'];
//}else{
//    header("Location: ../register.php");
//}
?>

<!DOCTYPE HTML>
<html>

<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="FirmBridge, Management System" />
    <meta name="author" content="Cleveon Africa Limited">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/dashboard/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../assets/dashboard/css/style.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="../assets/dashboard/css/bootstrap-select.css">
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="../assets/dashboard/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/firm.png">
    <link href="../assets/dashboard/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="../assets/dashboard/js/jquery-1.11.1.min.js"></script>
    <script src="../assets/dashboard/js/modernizr.custom.js"></script>
    <script src="../assets/dashboard/js/bootstrap-select.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="../assets/dashboard/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="../assets/dashboard/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="../assets/dashboard/js/metisMenu.min.js"></script>
    <script src="../assets/dashboard/js/custom.js"></script>
    <link href="../assets/dashboard/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
    <script src="../assets/dashboard/js/Chart.js"></script>
    <style>
        tr.booking th, tr.booking td{
            color:green;
        }
    </style>
</head>

<body class="cbp-spmenu-push">
    <div class="main-content">


      <!--left-fixed -navigation-->
          <div class=" sidebar" role="navigation">
              <div class="navbar-collapse">
                  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                      <ul class="nav" id="side-menu">
                          <li>
                              <a href="index.php" class="active"><i class="fa fa-home nav_icon"></i>Dashboard</a>
                          </li>
                           <li>
                              <a href="#"><i class="fa fa-envelope nav_icon"></i>Registered<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                  <li>
                             
                              <a href="registered-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Agents <span class="nav-badge-btm pull-right">22</span></a>
                          </li>
                          <li>
                             
                              <a href="registered-super-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Super Agents <span class="nav-badge-btm pull-right">14</span></a>
                          </li>
                          <li>
                             
                              <a href="registered-merchants.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Merchants <span class="nav-badge-btm pull-right">10</span></a>
                          </li>
                             <li>
                             
                              <a href="registered-sales.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Cells <span class="nav-badge-btm pull-right">18</span></a>
                          </li>
                              </ul>
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-envelope nav_icon"></i>Leads<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                        <li>
                             <a href="lead-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="lead-super-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="lead-merchants.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Merchants <span class="nav-badge-btm pull-right">3</span></a>
                          </li>
                        <li>
                             
                              <a href="lead-sales.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Cells <span class="nav-badge-btm pull-right">6</span></a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-envelope nav_icon"></i>Reports - Today<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="today-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                              <a href="today-super-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                              <a href="today-merchants.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                              <a href="today-sales.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                              <li>
                             
                              <a href="daily-targets.php" class="chart-nav"><i class="fa fa-bullseye nav_icon"></i>My Target</a>
                          </li>
                             <li>
                              <a href="daily-runrate.php" class="chart-nav">Monthly Run Rate</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-envelope nav_icon"></i>Pikash Reports - June<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                  <li>
                             <a href="monthly-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Agents <span class="nav-badge-btm pull-right">8</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-super-agents.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Super Agents <span class="nav-badge-btm pull-right">11</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-merchants.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Merchants <span class="nav-badge-btm pull-right">15</span></a>
                          </li>
                        <li>
                             
                              <a href="monthly-sales.php" class="chart-nav"><i class="glyphicon glyphicon-user nav_icon"></i>Cells <span class="nav-badge-btm pull-right">4</span></a>
                          </li>
                             <li>
                             
                              <a href="monthly-runrate.php" class="chart-nav">Yearly Run Rate <span class="nav-badge-btm pull-right">4</span></a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-envelope nav_icon"></i>Pikash Income<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                  <li>
                                      <a href="transactions.php">My Income</a>

                                  </li>

                              </ul>
                              <!-- //nav-second-level -->
                          </li>
                          <li>
                              <a href="registration-form.php" class=""><i class="fa fa-edit nav_icon"></i>Register Agent</a>
                          </li>

<!--
                          <li>
                              <a href="deposit.php">Deposit To Agent</a>
                          </li>
                          <li>
                              <a href="advance.php">Request Advance</a>
                          </li>
-->

                  </ul>
                      <!-- //sidebar-collapse -->
                  </nav>
              </div>
          </div>
          <!--left-fixed -navigation-->



          <!-- header-starts -->
          <style>
              @media(max-width:500px){
                    img.img-top{
                      width:50px !important;
                        height:50px !important;
                  }
                  .logo{
                      display: none;
                  }
                  }
          </style>
          <div class="sticky-header header-section ">
              <div class="header-left">
                  <!--toggle button start-->
                  <button id="showLeftPush"><i class="fa fa-bars"></i></button>
                  <!--toggle button end-->
                  <!--logo -->
                  <div class="logo">
                      <a href="index.php">
                      <img src="../assets/images/logo.png" alt="" class="" width="140" height="40">
          <!--
                          <h1>FirmBridge</h1>
                          <span>SystemPanel</span>
          -->
                      </a>
                  </div>
                  <!--//logo-->
              </div>
              <div class="header-right">

                  <div class="profile_details">
                      <ul>
                          <li class="dropdown profile_details_drop">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <div class="profile_img">
                                      <span class="prfil-img">
          <!--                            <img src="../user_profiles/client_3.jpg" alt="" width="50" height="50" style="border-radius:50%;" class="img-top">-->
                                       </span>
                                      <div class="user-name">
                                          <p>
                                             sales
                                          </p>
                                          <span>Administrator</span>
                                      </div>
                                      <i class="fa fa-angle-down lnr"></i>
                                      <i class="fa fa-angle-up lnr"></i>
                                      <div class="clearfix"></div>
                                  </div>
                              </a>
                              <ul class="dropdown-menu drp-mnu">

                                  <li> <a href="profile.php"><i class="fa fa-user"></i> Profile</a> </li>
                                  <li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="clearfix"> </div>
              </div>
              <div class="clearfix"> </div>
          </div>
          <!-- //header-ends -->
