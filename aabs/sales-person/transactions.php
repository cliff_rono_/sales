

  <?php
    include('header.php');
  ?>


        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
             
               <br>
                <div class="tables">
                    <div class="panel-body widget-shadow">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                           
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Transaction ID</th>
                                    
                                    <th>Amount</th>
                                    <th>Ratio</th>
                                    <th>Fee</th>
                                    <th>Income</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <!--footer-->
          <?php
            include('footer.php');
          ?>
        <!--//footer-->
