<?php 
ob_start();
session_start();
include "php_includes/db.php";
include "php_includes/functions.php";
if(isset($_GET['q21s']) && !isset($_GET['q22s'])){
    $county = $_GET['q21s'];
    $county = my_decrypt($county,$key);
    if($county){

    }else{
     header("Location: sitemap.php");
    }
}elseif(isset($_GET['q22s']) && isset($_GET['q21s'])){
    $status = $_GET['q22s'];
    $status = my_decrypt($status,$key);
    $county = $_GET['q21s'];
    $county = my_decrypt($county,$key);
    if($county){

    }else{
     header("Location: sitemap.php");
    }
    if($status){

    }else{
     header("Location: sitemap.php");
    }
}else{
    header("Location: sitemap.php");
}


?>
<!DOCTYPE HTML>
<html>

<head>
    <title>FirmBridge | <?php echo $county; ?> :: Cleveon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="FirmBridge, Management System" />
    <meta name="author" content="Cleveon Africa Limited">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/modernizr.custom.js"></script>

    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();

    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="js/metisMenu.min.js"></script>
    <script src="js/custom.js"></script>
    <link href="css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
</head>

<body class="cbp-spmenu-push">
    <div class="main-content">
        <!-- left-fixed-navigation -->
        <?php require_once "php_includes/left-fixed-navigation.php"; ?>
        <!-- //left-fixed-navigation -->
        <?php require_once "php_includes/header.php"; ?>
        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                <div class="tables">
                    <h3 class="title1">Sites
                        <?php if(isset($status)){echo " :: Viewing ".$status." Sites in ".$county;}else{} ?>
                    </h3>
                    <div class="profile_details">
                        <ul>
                            <li class="dropdown profile_details_drop">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <div class="profile_img">
                                        <div class="user-name">
                                            <p>Filter Results</p>
                                        </div>
                                        <i class="fa fa-angle-down lnr" style="margin-top:-3px;"></i>
                                        <i class="fa fa-angle-up lnr" style="margin-top:-3px;"></i>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                                <ul class="dropdown-menu drp-mnu">
                                    <li> <a href="county.php?q21s=<?php echo my_encrypt($county,$key); ?>&q22s=<?php echo my_encrypt('active',$key); ?>"><i class="fa fa-arrow-right"></i> View Active</a> </li>
                                    <li> <a href="county.php?q21s=<?php echo my_encrypt($county,$key); ?>&q22s=<?php echo my_encrypt('booked',$key); ?>"><i class="fa fa-arrow-right"></i> View Booked</a> </li>
                                    <li> <a href="county.php?q21s=<?php echo my_encrypt($county,$key); ?>&q22s=<?php echo my_encrypt('available',$key); ?>"><i class="fa fa-arrow-right"></i> View Available</a> </li>
                                    <li> <a href="county.php?q21s=<?php echo my_encrypt($county,$key); ?>&q22s=<?php echo my_encrypt('all',$key); ?>"><i class="fa fa-arrow-right"></i> View All</a> </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body widget-shadow">
                        <!--                        <h4>Basic Table:</h4>-->
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Site Id</th>
                                    <th>County</th>
                                    <th>Site</th>
                                    <th>Location</th>

                                    <th>Dimensions(M)</th>
                                    <th>Sides</th>
                                    <th>Current Client</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Duration</th>
                                    <!--                                    <th>Book Site</th>-->
                                    <th>All Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
    if(isset($_GET['q21s']) && !isset($_GET['q22s'])){
    $county = $_GET['q21s'];
    $county = my_decrypt($county,$key);
    $sql = "SELECT * FROM site_list WHERE county = '{$county}' ORDER BY id DESC";
                                $ex = mysqli_query($connection,$sql);
                                while($row = mysqli_fetch_assoc($ex)){
                                    $site_id = $row['site_id'];
                                    $county = $row['county'];
                                    $location = $row['location'];
                                    $length = $row['length'];
                                    $width = $row['width'];
                                    $site = $row['site'];
                                    $sides = $row['sides'];
                                    $bound_status = $row['bound_status'];
                                    $status = $row['status'];
                                    $query = "SELECT * FROM operations WHERE site_id = '$site_id' ORDER BY id DESC LIMIT 1";
                                    $result = mysqli_query($connection,$query);
                                    $count = mysqli_num_rows($result);
                                    if($count > 0){
                                     while($rows = mysqli_fetch_assoc($result)){
                                    $current_client = $rows['current_client'];
                                    $start_date = $rows['start_date'];
                                    $end_date = $rows['end_date'];
                                    $duration = $rows['duration'];
                                    }
                                    }else{
                                    $current_client = "None";
                                    $start_date = "Null";
                                    $end_date = "Null";
                                    $duration = "Zero";
                                    }
                                    ?>
                                    <tr>
                                        <th scope="row">
                                            <?php echo $site_id; ?>
                                        </th>
                                        <td>
                                            <?php echo $county; ?>
                                        </td>
                                        <td title="<?php echo $site; ?>">
                                            <?php
                                    if(strlen($site) >= 15){
                                       echo substr($site,0,15)."...";
                                    }else{
                                        echo $site;
                                    }
                                    ?>
                                        </td>
                                        <td title="<?php echo $location; ?>">
                                            <?php
                                    if(strlen($location) >= 15){
                                       echo substr($location,0,15)."...";
                                    }else{
                                        echo $location;
                                    }
                                    ?>
                                        </td>

                                        <td>
                                            <?php echo "( ".$length."m x ".$width."m )"; ?>
                                        </td>
                                        <td>
                                            <?php echo $sides." Sided"; ?>
                                        </td>
                                        <td>
                                            <?php echo $current_client; ?>
                                        </td>
                                        <td>
                                            <?php echo $start_date; ?>
                                        </td>
                                        <td>
                                            <?php echo $end_date; ?>
                                        </td>
                                        <td>
                                            <?php
                                    if($end_date == "Null"){
                                        $end_date = date("Y-m-d");
                                    }
                                    $days = date_difference($end_date,date("Y-m-d"));
                                      if(strpos($days,'-') !== false){
                                        $days = str_replace('-','',$days);
                                        $days = $days." remaining";
                                    }elseif(strpos($days,'+0') !== false){
                                        $days = str_replace('+','',$days);
                                        $days = $days." passed";
                                    }elseif(strpos($days,'+') !== false){
                                        $days = str_replace('+','-',$days);
                                        $days = $days." passed";
                                    }
                                    echo $days;
                                        ?>
                                        </td>
                                        <td><a href="single.php?site_id=<?php echo my_encrypt($site_id,$key); ?>">All Details</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                        <?php

}elseif(isset($_GET['q22s']) && isset($_GET['q21s'])){
    $status = $_GET['q22s'];
    $status = my_decrypt($status,$key);
    $county = $_GET['q21s'];
    $county = my_decrypt($county,$key);
        if($status == 'all'){
           $sql = "SELECT * FROM site_list WHERE county = '{$county}'";
        }else{
//            if($status == 'available'){
//            $sql = "SELECT * FROM site_list WHERE county = '{$county}' AND status = '$status'";
//            }elseif($status == 'booked'){
//             $sql = "SELECT * FROM site_list WHERE county = '{$county}'";
//            $result1 = mysqli_query($connection,$sql);
//                $array =  array();
//                while($row = mysqli_fetch_assoc($result1)){
//                    $site_id = $row['site_id'];
//                    array_push($array,$site_id);
//                }
//
//            }
         $sql = "SELECT * FROM site_list WHERE county = '{$county}' AND status = '$status'";
        }
                                $ex = mysqli_query($connection,$sql);
                                while($row = mysqli_fetch_assoc($ex)){
                                    $site_id = $row['site_id'];
                                    $county = $row['county'];
                                    $location = $row['location'];
                                    $length = $row['length'];
                                    $width = $row['width'];
                                    $site = $row['site'];
                                    $sides = $row['sides'];
                                    $bound_status = $row['bound_status'];
                                    $query = "SELECT * FROM operations WHERE site_id = '$site_id' ORDER BY id DESC LIMIT 1";
                                    $result = mysqli_query($connection,$query);
                                    $count = mysqli_num_rows($result);
                                    if($count > 0){
                                     while($rows = mysqli_fetch_assoc($result)){
                                    $current_client = $rows['current_client'];
                                    $start_date = $rows['start_date'];
                                    $end_date = $rows['end_date'];
                                    $duration = $rows['duration'];
                                    }
                                    }else{
                                    $current_client = "None";
                                    $start_date = "Null";
                                    $end_date = "Null";
                                    $duration = "Zero";
                                    }
                                    ?>
                                            <tr>
                                                <th scope="row">
                                                    <?php echo $site_id; ?>
                                                </th>
                                                <td>
                                                    <?php echo $county; ?>
                                                </td>
                                                <td>
                                                    <?php echo $site; ?>
                                                </td>
                                                <td>
                                                    <?php echo $location; ?>
                                                </td>

                                                <td>
                                                    <?php echo "( ".$length."m x ".$width."m )"; ?>
                                                </td>
                                                <td>
                                                    <?php echo $sides." Sided"; ?>
                                                </td>
                                                <td>
                                                    <?php echo $current_client; ?>
                                                </td>
                                                <td>
                                                    <?php echo $start_date; ?>
                                                </td>
                                                <td>
                                                    <?php echo $end_date; ?>
                                                </td>
                                                <td>
                                                    <?php echo $duration." days"; ?>
                                                </td>
                                                <td><a href="single.php?site_id=<?php echo my_encrypt($site_id,$key); ?>">All Details</a></td>
                                            </tr>
                                            <?php
                                }
}else{
    header("Location: sitemap.php");
}
                                ?>
                             <?php
                               ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--footer-->
        <div class="footer">
            <?php require_once "php_includes/developer.php"; ?>
        </div>
        <!--//footer-->
    </div>
    <!-- Classie -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/custom.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.table').dataTable();
        });
        var menuLeft = document.getElementById('cbp-spmenu-s1'),
            showLeftPush = document.getElementById('showLeftPush'),
            body = document.body;

        showLeftPush.onclick = function() {
            classie.toggle(this, 'active');
            classie.toggle(body, 'cbp-spmenu-push-toright');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            disableOther('showLeftPush');
        };

        function disableOther(button) {
            if (button !== 'showLeftPush') {
                classie.toggle(showLeftPush, 'disabled');
            }
        }

    </script>
    <!--scrolling js-->
    <script src="js/jquery.nicescroll.js"></script>
    <script src="js/scripts.js"></script>
    <!--//scrolling js-->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js">


    </script>
</body>

</html>
