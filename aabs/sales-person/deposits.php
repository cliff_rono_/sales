



  <?php
    include ('header.php');
  ?>


        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                <div class="row">
									    <div class="col-xs-12 col-sm-2 col-sm-offset-10">
									        <button class="btn btn-balance">
									            <span class="shown">Show Total</span>
									            <span class="hidden"><? //php echo "Ksh: ".User::getUserBalance($db,$agent_number,$key); ?></span>
									        </button>
									    </div>
									</div>
               <br>
                <div class="tables">
                    <div class="panel-body widget-shadow">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                 
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Transaction ID</th>
                                    <th>Agent Number</th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th>Float</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--footer-->
        <?php
          include('footer.php');
        ?>
        <!--//footer-->
