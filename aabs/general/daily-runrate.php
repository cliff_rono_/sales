<!DOCTYPE HTML>
<html>

<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="FirmBridge, Management System" />
    <meta name="author" content="Cleveon Africa Limited">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/dashboard/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../assets/dashboard/css/style.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="../assets/dashboard/css/bootstrap-select.css">
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="../assets/dashboard/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/firm.png">
    <link href="../assets/dashboard/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="../assets/dashboard/js/jquery-1.11.1.min.js"></script>
    <script src="../assets/dashboard/js/modernizr.custom.js"></script>
    <script src="../assets/dashboard/js/bootstrap-select.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="../assets/dashboard/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="../assets/dashboard/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="../assets/dashboard/js/metisMenu.min.js"></script>
    <script src="../assets/dashboard/js/custom.js"></script>
    <link href="../assets/dashboard/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
    <script src="../assets/dashboard/js/Chart.js"></script>
    <style>
        tr.booking th, tr.booking td{
            color:green;
        }
    </style>
</head>

<body class="cbp-spmenu-push">
    <div class="main-content">


      <!--left-fixed -navigation-->
          <div class=" sidebar" role="navigation">
              <div class="navbar-collapse">
                  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                      <ul class="nav" id="side-menu">
                          <li>
                              <a href="index.php" class="active"><i class="fa fa-home nav_icon"></i>Dashboard</a>
                          </li>

                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - Today<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="daily-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="daily-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="daily-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="daily-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="daily-reports-performance.php" class="chart-nav">Regional Performance</a>
                          </li>
                             <li>
                             
                              <a href="daily-runrate.php" class="chart-nav">Monthly Run Rate</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - June<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="monthly-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="monthly-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="monthly-reports-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                             <li>
                             
                              <a href="monthly-runrate.php" class="chart-nav">Yearly Run Rate</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - Overall<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="overall-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="overall-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="overall-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="overall-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="overall-reports-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Leads<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="leads-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="leads-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="leads-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="leads-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="leads-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                              </ul>
                              
                          </li>
                          
                          <li>
                              <a href="registration-form.php" class=""><i class="fa fa-edit nav_icon"></i>Register</a>
                          </li>

                  </ul>
                      <!-- //sidebar-collapse -->
                  </nav>
              </div>
          </div>
          <!--left-fixed -navigation-->
<!-- header-starts -->
          <style>
              @media(max-width:500px){
                    img.img-top{
                      width:50px !important;
                        height:50px !important;
                  }
                  .logo{
                      display: none;
                  }
                  }
          </style>
          <div class="sticky-header header-section ">
              <div class="header-left">
                  <!--toggle button start-->
                  <button id="showLeftPush"><i class="fa fa-bars"></i></button>
                  <!--toggle button end-->
                  <!--logo -->
                  <div class="logo">
                      <a href="index.php">
                      <img src="../assets/images/logo.png" alt="" class="" width="140" height="40">
          <!--
                          <h1>FirmBridge</h1>
                          <span>SystemPanel</span>
          -->
                      </a>
                  </div>
                  <!--//logo-->
              </div>
              <div class="header-right">

                  <div class="profile_details">
                      <ul>
                          <li class="dropdown profile_details_drop">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <div class="profile_img">
                                      <span class="prfil-img">
          <!--                            <img src="../user_profiles/client_3.jpg" alt="" width="50" height="50" style="border-radius:50%;" class="img-top">-->
                                       </span>
                                      <div class="user-name">
                                          <p>
                                             sales
                                          </p>
                                          <span>Administrator</span>
                                      </div>
                                      <i class="fa fa-angle-down lnr"></i>
                                      <i class="fa fa-angle-up lnr"></i>
                                      <div class="clearfix"></div>
                                  </div>
                              </a>
                              <ul class="dropdown-menu drp-mnu">

                                  <li> <a href="profile.php"><i class="fa fa-user"></i> Profile</a> </li>
                                  <li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="clearfix"> </div>
              </div>
              <div class="clearfix"> </div>
          </div>
          <!-- //header-ends -->
<!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                
               <br>
                <div class="tables">
                    <div class="panel-body widget-shadow">
                        <div id="chart_div"></div>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="application/javascript">
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawCurveTypes);
 
function drawCurveTypes() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Agents');
      data.addColumn('number', 'Super Agents');
        data.addColumn('number', 'Merchnats');
        data.addColumn('number', 'Cells');
      data.addRows([
        [0, 0, 0,0,30],    [1, 10, 5,1,30],   [2, 23, 15,2,30],  [3, 17, 9,3,30],   [4, 18, 10,5,30],  [5, 9, 5,6,30],
        [6, 11, 3,7,30],   [7, 27, 19,8,30],  [8, 33, 25,9,30],  [9, 40, 32,10,30],  [10, 32, 24,11,30], [11, 35, 27,12,30],
        [12, 30, 22,13,30], [13, 40, 32,14,30], [14, 42, 34,15,30], [15, 47, 39,16,30], [16, 44, 36,17,30], [17, 48, 40,18,30],
        [18, 52, 44,19,30], [19, 54, 46,20,30], [20, 42, 34,21,30], [21, 55, 47,22,30], [22, 56, 48,23,30], [23, 57, 49,24,30],
        [24, 60, 52,25,30], [25, 50, 42,26,30], [26, 52, 44,27,30], [27, 51, 43,28,30], [28, 49, 41,29,30], [29, 53, 45,30,30],
        [30, 55, 47,31,30]
      ]);

      var options = {
        hAxis: {
          title: 'Time'
        },
        vAxis: {
          title: 'Popularity'
        },
        series: {
          1: {curveType: 'function'}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
</script>
        <!--footer-->
        

<div class="footer">
<p>&copy; 2018 Pikash. All Rights Reserved.</p>
</div>

</div>
<!-- Classie -->
<script src="../assets/dashboard/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../assets/dashboard/vendors/jszip/dist/jszip.min.js"></script>
<script src="../assets/dashboard/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../assets/dashboard/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../assets/dashboard/js/classie.js"></script>
<script src="../assets/dashboard/js/custom.min.js"></script>
<script src="../js/pikash-forms.js"></script>
<script>
    $(document).ready(function() {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
            
        });

</script>
<script type="application/javascript">
$(document).ready(function() {
    $('.table').dataTable();
});
var menuLeft = document.getElementById('cbp-spmenu-s1'),
    showLeftPush = document.getElementById('showLeftPush'),
    body = document.body;

showLeftPush.onclick = function() {
    classie.toggle(this, 'active');
    classie.toggle(body, 'cbp-spmenu-push-toright');
    classie.toggle(menuLeft, 'cbp-spmenu-open');
    disableOther('showLeftPush');
};

function disableOther(button) {
    if (button !== 'showLeftPush') {
        classie.toggle(showLeftPush, 'disabled');
    }
}

</script>
<!--scrolling js-->
<script src="../assets/dashboard/js/jquery.nicescroll.js"></script>
<script src="../assets/dashboard/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="../assets/dashboard/js/bootstrap.js">


</script>
</body>

</html>

        <!--//footer-->
