<!DOCTYPE HTML>
<html>

<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="FirmBridge, Management System" />
    <meta name="author" content="Cleveon Africa Limited">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/dashboard/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../assets/dashboard/css/style.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="../assets/dashboard/css/bootstrap-select.css">
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="../assets/dashboard/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/firm.png">
    <link href="../assets/dashboard/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="../assets/dashboard/js/jquery-1.11.1.min.js"></script>
    <script src="../assets/dashboard/js/modernizr.custom.js"></script>
    <script src="../assets/dashboard/js/bootstrap-select.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="../assets/dashboard/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="../assets/dashboard/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="../assets/dashboard/js/metisMenu.min.js"></script>
    <script src="../assets/dashboard/js/custom.js"></script>
    <link href="../assets/dashboard/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
    <script src="../assets/dashboard/js/Chart.js"></script>
    <style>
        tr.booking th, tr.booking td{
            color:green;
        }
    </style>
</head>

<body class="cbp-spmenu-push">
    <div class="main-content">


      <!--left-fixed -navigation-->
          <div class=" sidebar" role="navigation">
              <div class="navbar-collapse">
                  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                      <ul class="nav" id="side-menu">
                          <li>
                              <a href="index.php" class="active"><i class="fa fa-home nav_icon"></i>Dashboard</a>
                          </li>

                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - Today<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="daily-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="daily-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="daily-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="daily-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="daily-reports-performance.php" class="chart-nav">Regional Performance</a>
                          </li>
                             <li>
                             
                              <a href="daily-runrate.php" class="chart-nav">Monthly Run Rate</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - June<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="monthly-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="monthly-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="monthly-reports-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                             <li>
                             
                              <a href="monthly-runrate.php" class="chart-nav">Yearly Run Rate</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - Overall<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="overall-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="overall-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="overall-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="overall-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="overall-reports-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Leads<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="leads-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="leads-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="leads-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="leads-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="leads-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                              </ul>
                              
                          </li>
                          
                          <li>
                              <a href="registration-form.php" class=""><i class="fa fa-edit nav_icon"></i>Register</a>
                          </li>

                  </ul>
                      <!-- //sidebar-collapse -->
                  </nav>
              </div>
          </div>
          <!--left-fixed -navigation-->
<!-- header-starts -->
          <style>
              @media(max-width:500px){
                    img.img-top{
                      width:50px !important;
                        height:50px !important;
                  }
                  .logo{
                      display: none;
                  }
                  }
          </style>
          <div class="sticky-header header-section ">
              <div class="header-left">
                  <!--toggle button start-->
                  <button id="showLeftPush"><i class="fa fa-bars"></i></button>
                  <!--toggle button end-->
                  <!--logo -->
                  <div class="logo">
                      <a href="index.php">
                      <img src="../assets/images/logo.png" alt="" class="" width="140" height="40">
          <!--
                          <h1>FirmBridge</h1>
                          <span>SystemPanel</span>
          -->
                      </a>
                  </div>
                  <!--//logo-->
              </div>
              <div class="header-right">

                  <div class="profile_details">
                      <ul>
                          <li class="dropdown profile_details_drop">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <div class="profile_img">
                                      <span class="prfil-img">
          <!--                            <img src="../user_profiles/client_3.jpg" alt="" width="50" height="50" style="border-radius:50%;" class="img-top">-->
                                       </span>
                                      <div class="user-name">
                                          <p>
                                             sales
                                          </p>
                                          <span>Administrator</span>
                                      </div>
                                      <i class="fa fa-angle-down lnr"></i>
                                      <i class="fa fa-angle-up lnr"></i>
                                      <div class="clearfix"></div>
                                  </div>
                              </a>
                              <ul class="dropdown-menu drp-mnu">

                                  <li> <a href="profile.php"><i class="fa fa-user"></i> Profile</a> </li>
                                  <li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="clearfix"> </div>
              </div>
              <div class="clearfix"> </div>
          </div>
          <!-- //header-ends -->
<!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                <div class="forms validation">
                    <div class="row">
                 <form data-toggle="validator" action="../form-processing/withdrawals.php" method="post">
                                
                                <br>
                                <br>
                                <div class="col-md-12 validation-grids validation-grids-left" style="width:100%;">
                                    <div class="widget-shadow" data-example-id="basic-forms">
                                        <div class="form-title">
                                            <h4>Registration Form</h4>
                                        </div>
                                        <div class="form-body" style="">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group has-feedback">
                                                <input type="text" name="agent_name" class="form-control" id="" placeholder="Agent Name" data-error="Sorry, this field is required" required >
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors">Agent Name</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group has-feedback">
                                                <input type="text" name="registration_date" class="form-control" id="" placeholder="Registration Date" data-error="Sorry, this field is required" required >
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors">Agent Registration Date</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group has-feedback">
                                                <input type="text" name="designated_phone_number" class="form-control" id="" placeholder="Mobile Number" data-error="Sorry, this field is required" required >
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors">Outlet's Designated Mobile Number</span>
                                                </div>
                                            </div>
                                             <div class="col-sm-8">
                                                <div class="form-group has-feedback">
                                                <input type="checkbox" name="super_agent" id="">Super Agent
                                                <input type="checkbox" name="agent" id="">Agent
                                                <input type="checkbox" name="merchant" id="">Merchant
                                                <input type="checkbox" name="cell" id="">Cell
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group has-feedback">
                                            <input type="text" name="preferred_username" class="form-control" id="" placeholder="Preferred Username" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Preferred Pikash Username</span>
                                        </div>
                                            </div>
                                        </div>
<!-- PROPRIETOR OR OWNER'S CONTACT-->
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="proprietor_location" class="form-control" id="" placeholder="Location" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">City/Town/Market</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-6">
                                        <select name="poraeh_region" class="form-control selectpicker" data-live-search="true" required>
                                            <option value="">Select Poraeh Region</option>
                                            <?php $html = '';
        $query = "SELECT code,province,name FROM counties ORDER BY code ASC";
        $stmt = mysqli_query($connection,$query);
        while($row = mysqli_fetch_assoc($stmt)){
            $name = $row['name'];
            $code = $row['code'];
            $province = $row['province'];
           echo '<option value="'.$code.'">'.$code.' - '.$name.' - '.$province.'</option>';
        } ?>
                                        </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="operator_building" class="form-control" id="" placeholder="Building" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Operator Location - Building</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="operator_road" class="form-control" id="" placeholder="Road" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Operator Location - Road</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="operator_street" class="form-control" id="" placeholder="Street" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Operator Location - Street</span>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="nearest_landmark" class="form-control" id="" placeholder="Building" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Nearest Landmark</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="landmark_postal_address" class="form-control" id="" placeholder="Postal Address" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Postal Address of nearest landmark</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="landmark_postal_code" class="form-control" id="" placeholder="Postal Code" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Postal Code For Landmark</span>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="proprietor_phone" class="form-control" id="" placeholder="Phone Number" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Contact Phone Number</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                             <div class="form-group has-feedback">
                                            <input type="text" name="proprietor_email" class="form-control" id="" placeholder="Email Address" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Contact Email</span>
                                        </div>
                                         </div>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_surname[]" class="form-control" id="" placeholder="Surname" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">1. Surname</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_middle_name[]" class="form-control" id="" placeholder="Middle Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">1. Middle Name</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_first_name[]" class="form-control" id="" placeholder="First Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">1. First Name</span>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                          <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_ID[]" class="form-control" id="" placeholder="Attendant 1 ID/Passport" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 1 ID/Passport</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_phone[]" class="form-control" id="" placeholder="Attendant 1 Phone" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 1 Phone</span>
                                        </div>
                                        </div> 
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_surname[]" class="form-control" id="" placeholder="Surname" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">2. Surname</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_middle_name[]" class="form-control" id="" placeholder="Middle Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">2. Middle Name</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_first_name[]" class="form-control" id="" placeholder="First Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">2. First Name</span>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                          <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_ID[]" class="form-control" id="" placeholder="Attendant 2 ID/Passport" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 2 ID/Passport</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_phone[]" class="form-control" id="" placeholder="Attendant 2 Phone" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 2 Phone</span>
                                        </div>
                                        </div> 
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_surname[]" class="form-control" id="" placeholder="Surname" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">3. Surname</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_middle_name[]" class="form-control" id="" placeholder="Middle Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">3. Middle Name</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_first_name[]" class="form-control" id="" placeholder="First Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">3. First Name</span>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                          <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_ID[]" class="form-control" id="" placeholder="Attendant 3 ID/Passport" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 3 ID/Passport</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_phone[]" class="form-control" id="" placeholder="Attendant 3 Phone" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 3 Phone</span>
                                        </div>
                                        </div> 
                                        </div>
                                        </div>
                                        </div>
                                        
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" name="add_site">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>

        <!--footer-->

        

<div class="footer">
<p>&copy; 2018 Pikash. All Rights Reserved.</p>
</div>

</div>
<!-- Classie -->
<script src="../assets/dashboard/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../assets/dashboard/vendors/jszip/dist/jszip.min.js"></script>
<script src="../assets/dashboard/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../assets/dashboard/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../assets/dashboard/js/classie.js"></script>
<script src="../assets/dashboard/js/custom.min.js"></script>
<script src="../js/pikash-forms.js"></script>
<script>
    $(document).ready(function() {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
            
        });

</script>
<script type="application/javascript">
$(document).ready(function() {
    $('.table').dataTable();
});
var menuLeft = document.getElementById('cbp-spmenu-s1'),
    showLeftPush = document.getElementById('showLeftPush'),
    body = document.body;

showLeftPush.onclick = function() {
    classie.toggle(this, 'active');
    classie.toggle(body, 'cbp-spmenu-push-toright');
    classie.toggle(menuLeft, 'cbp-spmenu-open');
    disableOther('showLeftPush');
};

function disableOther(button) {
    if (button !== 'showLeftPush') {
        classie.toggle(showLeftPush, 'disabled');
    }
}

</script>
<!--scrolling js-->
<script src="../assets/dashboard/js/jquery.nicescroll.js"></script>
<script src="../assets/dashboard/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="../assets/dashboard/js/bootstrap.js">


</script>
</body>

</html>


        <!--//footer-->
