<!DOCTYPE HTML>
<html>

<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="FirmBridge, Management System" />
    <meta name="author" content="Cleveon Africa Limited">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/dashboard/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../assets/dashboard/css/style.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="../assets/dashboard/css/bootstrap-select.css">
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="../assets/dashboard/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/firm.png">
    <link href="../assets/dashboard/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dashboard/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="../assets/dashboard/js/jquery-1.11.1.min.js"></script>
    <script src="../assets/dashboard/js/modernizr.custom.js"></script>
    <script src="../assets/dashboard/js/bootstrap-select.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="../assets/dashboard/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="../assets/dashboard/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="../assets/dashboard/js/metisMenu.min.js"></script>
    <script src="../assets/dashboard/js/custom.js"></script>
    <link href="../assets/dashboard/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
    <script src="../assets/dashboard/js/Chart.js"></script>
    <style>
        tr.booking th, tr.booking td{
            color:green;
        }
    </style>
</head>

<body class="cbp-spmenu-push">
    <div class="main-content">


      <!--left-fixed -navigation-->
          <div class=" sidebar" role="navigation">
              <div class="navbar-collapse">
                  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                      <ul class="nav" id="side-menu">
                          <li>
                              <a href="index.php" class="active"><i class="fa fa-home nav_icon"></i>Dashboard</a>
                          </li>

                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - Today<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="daily-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="daily-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="daily-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="daily-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="daily-reports-performance.php" class="chart-nav">Regional Performance</a>
                          </li>
                             <li>
                             
                              <a href="daily-runrate.php" class="chart-nav">Monthly Run Rate</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - June<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="monthly-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="monthly-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="monthly-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="monthly-reports-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                             <li>
                             
                              <a href="monthly-runrate.php" class="chart-nav">Yearly Run Rate</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Sales Reports - Overall<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="overall-reports-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="overall-reports-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="overall-reports-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="overall-reports-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="overall-reports-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                              </ul>
                              
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-folder-open nav_icon"></i>Leads<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level collapse">
                                 
                                 <li>
                             <a href="leads-agents.php" class="chart-nav">Agents <span class="nav-badge-btm pull-right">1</span></a>
                          </li>
                          <li>
                             
                              <a href="leads-super-agents.php" class="chart-nav">Super Agents <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                          <li>
                             
                              <a href="leads-merchants.php" class="chart-nav">Merchants <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                        <li>
                             
                              <a href="leads-cells.php" class="chart-nav">Cells <span class="nav-badge-btm pull-right">0</span></a>
                          </li>
                             <li>
                             
                              <a href="leads-performance.php" class="chart-nav">Team Performance</a>
                          </li>
                              </ul>
                              
                          </li>
                          
                          <li>
                              <a href="registration-form.php" class=""><i class="fa fa-edit nav_icon"></i>Register</a>
                          </li>

                  </ul>
                      <!-- //sidebar-collapse -->
                  </nav>
              </div>
          </div>
          <!--left-fixed -navigation-->
<!-- header-starts -->
          <style>
              @media(max-width:500px){
                    img.img-top{
                      width:50px !important;
                        height:50px !important;
                  }
                  .logo{
                      display: none;
                  }
                  }
          </style>
          <div class="sticky-header header-section ">
              <div class="header-left">
                  <!--toggle button start-->
                  <button id="showLeftPush"><i class="fa fa-bars"></i></button>
                  <!--toggle button end-->
                  <!--logo -->
                  <div class="logo">
                      <a href="index.php">
                      <img src="../assets/images/logo.png" alt="" class="" width="140" height="40">
          <!--
                          <h1>FirmBridge</h1>
                          <span>SystemPanel</span>
          -->
                      </a>
                  </div>
                  <!--//logo-->
              </div>
              <div class="header-right">

                  <div class="profile_details">
                      <ul>
                          <li class="dropdown profile_details_drop">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <div class="profile_img">
                                      <span class="prfil-img">
          <!--                            <img src="../user_profiles/client_3.jpg" alt="" width="50" height="50" style="border-radius:50%;" class="img-top">-->
                                       </span>
                                      <div class="user-name">
                                          <p>
                                             sales
                                          </p>
                                          <span>Administrator</span>
                                      </div>
                                      <i class="fa fa-angle-down lnr"></i>
                                      <i class="fa fa-angle-up lnr"></i>
                                      <div class="clearfix"></div>
                                  </div>
                              </a>
                              <ul class="dropdown-menu drp-mnu">

                                  <li> <a href="profile.php"><i class="fa fa-user"></i> Profile</a> </li>
                                  <li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="clearfix"> </div>
              </div>
              <div class="clearfix"> </div>
          </div>
          <!-- //header-ends -->

        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">

                <div class="elements  row">
                    <div class="col-md-4 profile widget-shadow">
                        <h4 class="title3">Profile</h4>
                        <div class="profile-top">
                            <img src="../assets/dashboard/user_profiles/profile.png" width="100" height="100" alt="">
                            <h4>
                                Pikash
                            </h4>
                            <h5>Full Names</h5>
                        </div>
                        <div class="profile-text">
                            <div class="profile-row">
                                <div class="profile-left">
                                    <i class="fa fa-envelope profile-icon"></i>
                                </div>
                                <div class="profile-right">
                                    <h4>
                                        someone@example.com </h4>
                                    <p>Email</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="profile-row row-middle">
                                <div class="profile-left">
                                    <i class="fa fa-mobile profile-icon"></i>
                                </div>
                                <div class="profile-right">
                                    <h4>
                                        +25412345678
                                    </h4>
                                    <p>Contact Number</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="profile-row">
                                <div class="profile-left">
                                    <i class="fa fa-user profile-icon"></i>
                                </div>
                                <div class="profile-right">
                                    <h4 id="details">
                                        Full names again
                                    </h4>
                                    <p>Full Name</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="profile-btm" style="padding:0px;">
                            <a id="success" href="#mymodal1" data-toggle="modal" data-target="#mymodal1" class="btn btn-primary btn-block" style="border-radius:0px;padding:20px 10px;">Change My Password</a>
                        </div>
                    </div>
                    <input type="hidden" name="" id="user" value="5">

					<div class="clearfix"> </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="mymodal1">
            <div class="modal-dialog">
                <div class="modal-content" style="background:#F1F1F1;border-radius:0px;">
                    <div class="modal-header" style="border:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="close" style="color:#000;">x</button>
                        <h2 class="text-center" style="color:#000;">Change Password</h2>
                    </div>
                    <form data-toggle="validator" action="" method="post" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="modal-body" style="border:none;">
                                <div class="form-group has-feedback">
                                    <input type="password" name="old_pass" class="form-control" id="old_pass" placeholder="Old Password" data-error="Sorry, this field is required" required>
                                    <span class="error"> </span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" name="new_pass" class="form-control" id="pass1" placeholder="New Password" data-error="Sorry, this field is required" required>
                                    <span class="error"> </span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" name="new_pass2" class="form-control" id="pass2" placeholder="Confirm New Password" data-error="Sorry, this field is required" required>
                                    <span class="error" id="passErr" style="color:red;display:none;font-weight:bold;font-size:18px;"></span>
                                    <span class="success" id="passsuccess" style="color:orangered;display:none;font-weight:bold;font-size:18px;"></span>
                                </div>
                            </div>
                            <div class="modal-footer" style="border:none;">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="form-group">
                                            <input type="submit" name="update_pass" value="Change Password" class="btn btn-success btn-block" style="padding:15px 10px;border-radius:0px;" id="conf_pass">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!--footer-->
        <?php
          include('footer.php');
      ?>
        <!--//footer-->
