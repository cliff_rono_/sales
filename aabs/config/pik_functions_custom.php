<?php

class POC{
    public static function getEmailInstance($email,$db){
     $condition = false;
    ##USER INNER JOINS TO SELECT ONCE
        
    
   ##CHECK IF EMAIL ADDRESS IS REGISTERED FOR A USER
    $sql = "SELECT id FROM user WHERE email = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
        
        
    }else{
     ##CHECK IN AGENTS
    $sql = "SELECT id FROM agent WHERE email = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
       
    }else{
    ##CHECK IN MERCHANTS
    $sql = "SELECT id FROM merchants WHERE email = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
        
    }else{
    ##CHECK IN SUPER USERS
    $sql = "SELECT id FROM super_agent WHERE email = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
        
    }else{
    $condition = true;
    
    }
    }
    }
    } 
        return $condition;
    }
    
    
    
    public static function getEmail2Instance($email,$db){
     $condition = "default false";
    
        
        ##USER INNER JOINS TO SELECT ONCE
        
    
   ##CHECK IF EMAIL ADDRESS IS REGISTERED FOR A USER
    $sql = "SELECT id FROM user WHERE phone = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
        while($row = $stmt->fetch(PDO::FETCH_OBJ)){
            $id = $row->id;
        }
 $condition = "default false";  
    }else{
     ##CHECK IN AGENTS
    $sql = "SELECT id FROM agent WHERE phone = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
     $condition = "default false";  
    }else{
    ##CHECK IN MERCHANTS
    $sql = "SELECT id FROM merchants WHERE phone = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
     $condition = "default false";   
    }else{
    ##CHECK IN SUPER USERS
    $sql = "SELECT id FROM super_agent WHERE phone = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
       $condition = "default false"; 
    }else{
    $condition = "default true";
    
    }
    }
    }
    } 
        return $condition;
    }

    
    ##deposit to agent
    public static function AgentDeposit($user_id,$agent_id,$amount,$db,$connection,$key){
    //$user_id = intval($user_id);
    $query = "SELECT amount FROM pikash_account WHERE user_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$user_id]);
        if($stmt->rowCount() < 1){
            die("Sorry, this user is not registered. kindly register user first");
        }
    while($row = $stmt->fetch(PDO::FETCH_OBJ)){
        $my_balance = my_decrypt($row->amount,$key);
    }
    
    ##CHECK AGENT FLOAT
    $query = "SELECT account_balance FROM agents_pikash_accounts WHERE agent_id = '$agent_id'";
    $result = mysqli_query($connection,$query);
    if(!$result){
        die(mysqli_error("An Error Occured"));
    }
 
    while($row = mysqli_fetch_assoc($result)){
        $agent_balance = my_decrypt($row['account_balance'],$key);
    }
    
    if($agent_balance < $amount){
        die("This transaction will not be possible, kindly top your pikash account.");
    } 
    
    ##INCREASE AGENT AMOUNT
    $new_agent_amount = $agent_balance - $amount;
    $new_agent_amount1 = my_encrypt($new_agent_amount,$key);
    $new_user_amount = $my_balance + $amount;
    $new_user_amount1 = my_encrypt($new_user_amount,$key);
    
    ##UPDATEUSERAMOUNT
    
    $query = "UPDATE pikash_account SET amount = ? WHERE user_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$new_user_amount1,$user_id]);
    
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO transactions(transaction_id,transaction_type,amount,receiver_id,narration,balance_after) VALUES(?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,'deposit',$amount,$user_id,'Deposit at an agent outlet',$new_user_amount]);
    
    ##UPDATE AGENT AMOUNT
    $query = "UPDATE agents_pikash_accounts SET account_balance = '$new_agent_amount1' WHERE agent_id = $agent_id";
    $result = mysqli_query($connection,$query);
    if(!$result){
        die(mysqli_error($connection));
    }
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO agent_transactions(transaction_id,agent_number,type,user_id,amount,balance_after) VALUES(?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,$agent_id,'deposit',$user_id,$amount,$new_agent_amount]);
    
    
    $query = "SELECT name FROM user WHERE id = $user_id";
    $result = mysqli_query($connection,$query);
    while($row = mysqli_fetch_assoc($result)){
        
        $user_name = $row['name'];
        
    }
    die("$transaction_id Confirmed. You have deposited ksh: $amount.00 to $user_name on " .date("Y-m-d H:i:s"). " New Pikash Balance is Ksh: $new_agent_amount.00.");
    
}
    
    
public static function withDrawFromAgent($user_id,$agent_id,$amount,$db,$connection,$key){
    
    $query = "SELECT amount FROM pikash_account WHERE user_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$user_id]);
    while($row = $stmt->fetch(PDO::FETCH_OBJ)){
        $my_balance = my_decrypt($row->amount,$key);
    }
    ##check withdrawal fee
        $query = "SELECT fee FROM pikash_pricing WHERE transaction_type = 'withdraw'";
        $result = mysqli_query($connection,$query);
        while($row = mysqli_fetch_assoc($result)){
            $fee = $row['fee'];
        }
    $threshold = $amount + $fee;
    if($my_balance < $amount){
        die("You do not have sufficient balance to make this transaction.");
    }
    if($my_balance < $threshold){
        die("Cannot complete transaction, you will need to pay a transaction fee of Ksh: ".$fee);
    }
    
    
    
    
    ##record pikash earning
//    $query = "INSERT INTO pikash_earnings(transaction_type,amount,initiator,initiator_type,outlet) VALUES('withdraw',$fee,$user_id,'user',$agent_id)";
//        $result = mysqli_query($connection,$query);
    
    ##CHECK AGENT FLOAT
    $query = "SELECT account_balance FROM agents_pikash_accounts WHERE agent_id = $agent_id";
    $result = mysqli_query($connection,$query);
    if(!$result){
        die(mysqli_error($connection));
    }
    $count = mysqli_num_rows($result);
    if($count < 1){
        die("Unknown Agent Number Provided ".$agent_id);
    }else{
        
    }
    while($row = mysqli_fetch_assoc($result)){
        $agent_balance = my_decrypt($row['account_balance'],$key);
    }
    
    if($agent_balance < $amount){
        die("This transaction will not be possible, kindly enquire from the agent and try again.");
    } 
    
    ##INCREASE AGENT AMOUNT
    $new_agent_amount = $agent_balance + $amount;
    $new_agent_amount1 = my_encrypt($new_agent_amount,$key);
    $new_user_amount = $my_balance - $threshold;
    $new_user_amount1 = my_encrypt($new_user_amount,$key);
    
    ##UPDATEUSERAMOUNT
    
    $query = "UPDATE pikash_account SET amount = ? WHERE user_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$new_user_amount1,$user_id]);
    
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO transactions(transaction_id,transaction_type,amount,receiver_id,narration,balance_after) VALUES(?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,'withdrawal',$amount,$user_id,'Withdrawal from agent',$new_user_amount]);
    
    $query = "SELECT name,super_agent FROM agent WHERE agent_number = $agent_id";
    $result = mysqli_query($connection,$query);
    while($row = mysqli_fetch_assoc($result)){
        $agent_name = $row['name'];
        $super_agent = $row['super_agent'];
        
    }
    
    
    ##USE USER TRANSACTION_ID TO SHARE FEE
    ##check transaction sharing ratios
    $query = "SELECT * FROM fee_shares WHERE nature = 'withdrawal'";
    $result = mysqli_query($connection,$query);
    $row = mysqli_fetch_assoc($result);
    $pikash_percentage = $row['pikash'];
    $super_agent_percentage = $row['super_agent'];
    $agent_percentage = $row['agent'];
    
    ##check if exists superAgent
    if($super_agent){
        $fee_sa = ($super_agent_percentage/100)*$fee;
        $fee_pk = ($pikash_percentage/100)*$fee;
        $fee_ag = ($agent_percentage/100)*$fee;
        
        
        ##CREDIT SUPER AGENT
        //$sa_trans_id = strtoupper(uniqid());
        $query = "INSERT INTO super_agent_earnings(super_agent_id,transaction_id,amount,ratio,fee,income) ";
        $query .= "VALUES($super_agent,'$transaction_id',$amount,$super_agent_percentage,$fee,$fee_sa)";
        $result = mysqli_query($connection,$query);
        
        ##CREDIT AGENT
        $sa_trans_id = strtoupper(uniqid());
        $query = "INSERT INTO agent_earnings(agent_number,transaction_id,amount,ratio,fee,income) ";
        $query .= "VALUES($agent_id,'$transaction_id',$amount,$agent_percentage,$fee,$fee_ag)";
        $result = mysqli_query($connection,$query);
        
        ##CREDIT PIKASH
        ##COLLECT PIKASH FEE
    $query = "INSERT INTO pikash_earnings(transaction_type,amount,initiator,initiator_type,outlet) VALUES('withdraw',$fee_pk,$user_id,'user',$agent_id)";
        $result = mysqli_query($connection,$query);
    }else{
        
        $fee_pk = (50/100)*$fee;
        $fee_ag = (50/100)*$fee;
        
        
        ##CREDIT AGENT
        $sa_trans_id = strtoupper(uniqid());
        $query = "INSERT INTO agent_earnings(agent_number,transaction_id,amount,ratio,fee,income) ";
        $query .= "VALUES($agent_id,'$transaction_id',$amount,50,$fee,$fee_ag)";
        $result = mysqli_query($connection,$query);
        
        ##CREDIT PIKASH
        ##COLLECT PIKASH FEE
    $query = "INSERT INTO pikash_earnings(transaction_type,amount,initiator,initiator_type,outlet) VALUES('withdraw',$fee_pk,$user_id,'user',$agent_id)";
        $result = mysqli_query($connection,$query);
    }
    
    
    
    ##UPDATE AGENT AMOUNT
    $query = "UPDATE agents_pikash_accounts SET account_balance = '$new_agent_amount1' WHERE agent_id = $agent_id";
    $result = mysqli_query($connection,$query);
    if(!$result){
        die(mysqli_error($connection));
    }
    
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO agent_transactions(transaction_id,agent_number,type,user_id,amount,balance_after) VALUES(?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,$agent_id,'withdrawal',$user_id,$amount,$new_agent_amount]);
    
    
    
    die("$transaction_id Confirmed. You have withdrawn ksh: $amount.00 from $agent_name on " .date("Y-m-d H:i:s"). " New Pikash Balance is Ksh: $new_user_amount.00. Transaction fee Ksh: $fee.00");
    
}



 
##PAY BILLS
public static function payBill($user_id,$merchant_code,$amount,$db,$connection,$key,$narration){
    
    $query = "SELECT amount FROM pikash_account WHERE user_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$user_id]);
    while($row = $stmt->fetch(PDO::FETCH_OBJ)){
        $my_balance = my_decrypt($row->amount,$key);
    }
    ##check withdrawal fee
        $query = "SELECT fee FROM pikash_pricing WHERE transaction_type = 'bill'";
        $result = mysqli_query($connection,$query);
        while($row = mysqli_fetch_assoc($result)){
            $fee = $row['fee'];
        }
    $threshold = $amount + $fee;
    
    if($my_balance < $amount){
        die("You do not have sufficient balance to make this transaction.");
    }
    if($my_balance < $threshold){
        die("Cannot complete transaction, you should be able to pay a fee of Ksh: ".$fee);
    }
    
    ##record pikash earning
    $query = "INSERT INTO pikash_earnings(transaction_type,amount,initiator,initiator_type,outlet) VALUES('bill',$fee,$user_id,'user',$merchant_code)";
        $result = mysqli_query($connection,$query);
    
    ##CHECK AGENT FLOAT
    $query = "SELECT account_balance FROM merchants_pikash_accounts WHERE merchant_id = $merchant_code";
    $result = mysqli_query($connection,$query);
    if(!$result){
        die(mysqli_error($connection));
    }else{
        
    }
    $count = mysqli_num_rows($result);
    if($count < 1){
        die("Unknown Agent Number Provided".$agent_id);
    }else{
        
    }
    while($row = mysqli_fetch_assoc($result)){
        $agent_balance = my_decrypt($row['account_balance'],$key);
    } 
    
    ##INCREASE AGENT AMOUNT
    $new_agent_amount = $agent_balance + $threshold;
    $new_agent_amount1 = my_encrypt($new_agent_amount,$key);
    $new_user_amount = $my_balance - $amount;
    $new_user_amount1 = my_encrypt($new_user_amount,$key);
    
    ##UPDATEUSERAMOUNT
    
    $query = "UPDATE pikash_account SET amount = ? WHERE user_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$new_user_amount1,$user_id]);
    
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO transactions(transaction_id,transaction_type,amount,receiver_id,narration,balance_after) VALUES(?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,'withdrawal',$amount,$user_id,'Payment made to merchant code '.$merchant_code,$new_user_amount]);
    
    ##UPDATE AGENT AMOUNT
    
    $query = "UPDATE merchants_pikash_accounts SET account_balance = ? WHERE merchant_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$new_agent_amount1,$merchant_code]);
    
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO merchant_transactions(transaction_id,merchant_id,type,user_id,amount,balance_after) VALUES(?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,$merchant_code,'payment',$user_id,$amount,$new_agent_amount]);
    
    
    $query = "SELECT name FROM merchants WHERE code = $merchant_code";
    $result = mysqli_query($connection,$query);
    while($row = mysqli_fetch_assoc($result)){
        $agent_name = $row['name'];
        
    }
    die("$transaction_id Confirmed. You have paid ksh: $amount.00 to $agent_name code $merchant_code on " .date("Y-m-d H:i:s"). " New Pikash Balance is Ksh: $new_user_amount.00. Transaction fee Ksh: $fee.00");
    
}

    
    
    
##PAY BILLS
public static function merchantPayBill($merchant_id,$merchant_code,$amount,$db,$connection,$key,$narration){
$query = "SELECT code FROM merchants WHERE id = ?";
$stmt = $db->prepare($query);
if($stmt->execute([$merchant_id])){
while($row = $stmt->fetch(PDO::FETCH_OBJ)){

$merchant_code1 = $row->code;
}
}
    ##check paying merchant account balance
$sql = "SELECT account_balance FROM merchants_pikash_accounts WHERE merchant_id = ?";
$stmt2 = $db->prepare($sql);
$stmt2->execute([$merchant_code1]);
while($row2 = $stmt2->fetch(PDO::FETCH_OBJ)){
$sender_amount = $row2->account_balance;
}
$sender_amount = my_decrypt($sender_amount,$key);
##check withdrawal fee
        $query = "SELECT fee FROM pikash_pricing WHERE transaction_type = 'bill'";
        $result = mysqli_query($connection,$query);
        while($row = mysqli_fetch_assoc($result)){
            $fee = $row['fee'];
        }
    $threshold = $amount + $fee;
if($sender_amount > $amount){
    
}else{
    ##SENDER IS BROKE
    die("You do not have enough balance to make this transaction");
}
    if($sender_amount > $threshold){
        
    }else{
        die("Request not successful, you will need to pay a fee of Ksh: ".$fee);
    }
    
    ##record pikash earning
    $query = "INSERT INTO pikash_earnings(transaction_type,amount,initiator,initiator_type,outlet) VALUES('bill',$fee,$merchant_code1,'merchant',$merchant_code)";
        $result = mysqli_query($connection,$query);
    
    ##CHECK AGENT FLOAT
    $query = "SELECT account_balance FROM merchants_pikash_accounts WHERE merchant_id = $merchant_code";
    $result = mysqli_query($connection,$query);

    while($row = mysqli_fetch_assoc($result)){
        $sent_balance = my_decrypt($row['account_balance'],$key);
    } 
    
    ##INCREASE AGENT AMOUNT
    $new_sent_amount = $sent_balance + $amount;
    $new_sent_amount1 = my_encrypt($new_sent_amount,$key);
    $new_sender_amount = $sender_amount - $threshold;
    $new_sender_amount1 = my_encrypt($new_sender_amount,$key);
    
    ##UPDATEUSERAMOUNT
    $query = "SELECT name FROM merchants WHERE code = $merchant_code";
    $result = mysqli_query($connection,$query);
    while($row = mysqli_fetch_assoc($result)){
        $agent_name = $row['name'];
        
    }
    $query = "SELECT name FROM merchants WHERE code = $merchant_code1";
    $result = mysqli_query($connection,$query);
    while($row = mysqli_fetch_assoc($result)){
        $agent_name2 = $row['name'];
        
    }
    ##UPDATE AGENT AMOUNT
    
    $query = "UPDATE merchants_pikash_accounts SET account_balance = ? WHERE merchant_id = ?";
    $stmt = $db->prepare($query);
    $stmt->execute([$new_sent_amount1,$merchant_code]);
    
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO merchant_transactions(transaction_id,merchant_id,type,user_id,amount,reason,balance_after) VALUES(?,?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,$merchant_code,'payment',$merchant_code1,$amount,'Bill payment from '.$agent_name2,$new_sent_amount]);
    
        ##SEND MESSAGE TO RECEIVING MERCHANT
    
        $query = "UPDATE merchants_pikash_accounts SET account_balance = ? WHERE merchant_id = ?";
        $stmt = $db->prepare($query);
        $stmt->execute([$new_sender_amount1,$merchant_code1]);
    
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO merchant_transactions(transaction_id,merchant_id,type,user_id,amount,reason,balance_after) VALUES(?,?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,$merchant_code1,'Bill Payment',$merchant_code,$amount,'Bill payment to '.$agent_name,$new_sender_amount]);
    
    

    die("$transaction_id Confirmed. You have paid ksh: $amount.00 to $agent_name code $merchant_code on " .date("Y-m-d H:i:s"). " New Pikash Balance is Ksh: $new_sender_amount.00. Transaction fee Ksh: $fee.00");
    
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}