

    <?php
      include('header.php');
    ?>

        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                <div class="forms validation">
                    <div class="row">
                 <form data-toggle="validator" action="../form-processing/agent-load.php" method="post">
                                <a id="success" class="hiden" href="#mymodal" data-toggle="modal" data-target="#mymodal"></a>
                                <br>
                                <br>
                                <div class="col-md-6 col-md-offset-3 validation-grids validation-grids-right">
                                    <div class="widget-shadow" data-example-id="basic-forms">
                                        <div class="form-title">
                                            <h4>Make Deposit To Agent</h4>
                                        </div>
                                        <div class="form-body" style="">

                                        <div class="form-group has-feedback">
                                            <select name="agent_number" class="form-control selectpicker" data-live-search="true" data-error="Sorry, This is a required field" required >
                                            <option value="">Select Agent</option>
                                            <?php $html = '';
                                            $query = "SELECT * FROM agent WHERE super_agent = $super_agent_id";
                                            $stmt = mysqli_query($connection,$query);
                                            while($row = mysqli_fetch_assoc($stmt)){
                                                $name = $row['name'];
                                                $code = $row['agent_number'];
                                               echo '<option value="'.$code.'">'.$name.' - '.$code.'</option>';
                                            } ?>
                                        </select>
                                        </div>
                                       <div class="form-group has-feedback">
                                            <input type="text" name="amount" class="form-control" id="" placeholder="Amount" data-error="Sorry, provide deposit amount" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Amount To Deposit</span>
                                        </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" name="add_site">Deposit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
       
        <!--footer-->

        <?php
          include('footer.php');
        ?>

        <!--//footer-->
