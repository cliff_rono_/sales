<?php 
ob_start();
session_start();
require_once "php_includes/db.php";
require_once "php_includes/functions.php";
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>FirmBridge | Profile Edit :: Cleveon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="FirmBridge, Management System" />
    <meta name="author" content="Cleveon Africa Limited">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- Bootstrap Core CSS -->
    <link rel="shortcut icon" href="images/firm.png">
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <!--//js-->
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();

    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="js/metisMenu.min.js"></script>
    <script src="js/custom.js"></script>
    <link href="css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
        <style>
    #page-wrapper {
    padding: 4em 2em 1.5em;
    background-color: #F1F1F1;
}
    </style>
</head>

<body class="cbp-spmenu-push">
    <div class="main-content">
        <!-- left-fixed-navigation -->
        <?php require_once "php_includes/left-fixed-navigation.php"; ?>
        <!-- //left-fixed-navigation -->
        <?php require_once "php_includes/header.php"; ?>
        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                <div class="forms validation">
                    <!--                    <h3 class="title1">Edit Your Profile</h3>-->
                    <div class="row">
                        <?php
$firstnameErr = "";
$lastnameErr = "";
$emailErr = "";
$genderErr = "";
if(isset($_POST['update_profile'])){
//sanitize and clean
if(isset($_POST['first_name'])){
if(!empty($_POST['first_name']) && !ctype_space($_POST['first_name'])){
$first_name = clean($_POST['first_name']);
//remove all illegal characters from this string
$first_name = preg_replace('/[^a-zA-Z0-9 .-]/','',$first_name);
}else{
$firstnameErr = "Kindly fill out this field.";
}
}
        //sanitize and clean
if(isset($_POST['phone'])){
if(!empty($_POST['phone']) && !ctype_space($_POST['phone'])){
$phone = clean($_POST['phone']);
//remove all illegal characters from this string
$phone = preg_replace('/[^a-zA-Z0-9 .-]/','',$phone);
}else{
$phoneErr = "Kindly fill out this field.";
}
}
//sanitize and clean
if(isset($_POST['last_name'])){
if(!empty($_POST['last_name']) && !ctype_space($_POST['last_name'])){
$last_name = clean($_POST['last_name']);
//remove all illegal characters from this string
$last_name = preg_replace('/[^a-zA-Z0-9 .-]/','',$last_name);
}else{
$lastnameErr = "Kindly fill out this field.";
}
}
//sanitize and clean if(isset($_POST['phone'])){ if(!empty($_POST['phone']) && !ctype_space($_POST['phone'])){ $phone = clean($_POST['phone']); //remove all illegal characters from this string $phone = preg_replace('/[^a-zA-Z0-9 .-]/','',$phone); }else{ $phoneErr = "Kindly fill out this field."; } }
//sanitize and clean ad title
if(isset($_POST['email'])){
if(!empty($_POST['email']) && !ctype_space($_POST['email'])){ 
$email = $_POST['email'];
$email = filter_var($email,FILTER_SANITIZE_EMAIL);
if(filter_var($email,FILTER_VALIDATE_EMAIL)){
    $query = "SELECT * FROM users WHERE email = '$email' AND id != $user_id";
    $result = mysqli_query($connection,$query);
    $count = mysqli_num_rows($result);
    if($count > 0){
      $emailErr = "Sorry, This email address is already registered!";  
    }
    
}else{
    $emailErr = "Invalid Email Address!";
}
}else{
$emailErr = '';  
}
}
            if(empty($firstnameErr) && empty($lastnameErr) && empty($emailErr) && empty($genderErr)){
        $query = "UPDATE users SET first_name = '$first_name',last_name = '$last_name',email='$email',phone = '$phone' WHERE id = $user_id";      
        $result = mysqli_query($connection,$query);
        if($result){
            $_SESSION['email'] = $email;
            //$user_id = mysqli_insert_id($connection);
            $image = $_FILES['image']['name'];
            $img_tmp = $_FILES['image']['tmp_name'];
            if($image != ''){
                unlink('user_profiles/'.$user_image);
                if(move_uploaded_file($img_tmp,"user_profiles/".$image)){
                $query3 = "UPDATE users SET user_image = '$image' WHERE id = $user_id";
                $query3ex = mysqli_query($connection,$query3);
            }
            }
            ?>
                            <script>
                                $(document).ready(function() {
                                    $('#success').click();
                                });

                            </script>
                            <?php
        }else{
            die('error'.mysqli_error($connection));
        }
    }else{
          die('errors'.$emailErr);     
            }

    
    
}
?>
                                <form data-toggle="validator" action="" method="post" enctype="multipart/form-data">
                                    <div class="col-md-6 col-md-offset-3 validation-grids widget-shadow" data-example-id="basic-forms">
                                        <div class="form-title">
                                            <h4>Profile Details</h4>
                                        </div>
                                        <div class="form-body">

                                            <div class="form-group has-feedback">
                                                <input type="text" name="first_name" class="form-control" id="" placeholder="First Name" data-error="Sorry, this field is required" required value="<?php echo $first_name; ?>">
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" name="last_name" class="form-control" id="" placeholder="Last Name" data-error="Sorry, this field is required" required value="<?php echo $last_name; ?>">
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" name="phone" class="form-control" id="" placeholder="Contact Phone" data-error="Sorry, this field is required" required value="<?php echo $phone; ?>">
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="text" name="email" class="form-control" id="" placeholder="Email Address" data-error="Sorry, this field is required" required value="<?php echo $_SESSION['email']; ?>">
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                            </div>
                                            <div class="form-group has-feedback">
                                                <input type="file" name="image" class="form-control" style="display:none;" id="image_upload">
                                                <button type="button" id="image_uploader" value="" class="btn btn-success btn-block"><i class="fa fa-upload"></i> Change Profile Image</button>

                                            </div>

                                            <a id="success" class="hiden" href="#mymodal" data-toggle="modal" data-target="#mymodal"></a>
                                            <div class="form-group">
                                                <input type="submit" name="update_profile" value="Update Profile" class="btn btn-primary">
                                            </div>

                                        </div>
                                    </div>
                                </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mymodal">
            <div class="modal-dialog">
                <div class="modal-content" style="background:rgba(0,0,0,0.9);border-radius:0px;">
                    <div class="modal-header" style="border:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="close" style="color:#fff;">x</button>
                        <h2 class="text-center" style="color:#fff;">Operation Successful!</h2>
                    </div>
                    <div class="modal-body" style="border:none;">
                        <p class="text-center text-info" style="font-weight:bold;font-size:20px;">Your Profile Details Have Been Updated!</p>
                    </div>
                    <div class="modal-footer" style="border:none;">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="profile.php" class="btn btn-danger btn-block" style="border-radius:0px;font-weight:bold;font-size:18px;margin-bottom:10px;">View Profile</a>
                            </div>
                            <div class="col-sm-6">
                                <a href="index.php" class="btn btn-danger btn-block" style="border-radius:0px;font-weight:bold;font-size:18px;margin-bottom:10px;">Home Page</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer-->
        <div class="footer">
            <?php require_once "php_includes/developer.php"; ?>
        </div>
        <!--//footer-->
    </div>
    <!-- Classie -->
    <script src="js/classie.js"></script>
    <script>
        $(document).ready(function() {
            $('#image_uploader').click(function(e) {
                e.preventDefault();
                $('#image_upload').click();
            });
        });
        var menuLeft = document.getElementById('cbp-spmenu-s1'),
            showLeftPush = document.getElementById('showLeftPush'),
            body = document.body;

        showLeftPush.onclick = function() {
            classie.toggle(this, 'active');
            classie.toggle(body, 'cbp-spmenu-push-toright');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            disableOther('showLeftPush');
        };

        function disableOther(button) {
            if (button !== 'showLeftPush') {
                classie.toggle(showLeftPush, 'disabled');
            }
        }

    </script>
    <!--scrolling js-->
    <script src="js/jquery.nicescroll.js"></script>
    <script src="js/scripts.js"></script>
    <!--//scrolling js-->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js">


    </script>
    <!--validator js-->
    <script src="js/validator.min.js"></script>
    <!--//validator js-->
</body>

</html>
