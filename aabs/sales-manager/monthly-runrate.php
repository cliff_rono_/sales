

  <?php
    include ('header.php');
  ?>


        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                
               <br>
                <div class="tables">
                    <div class="panel-body widget-shadow">
                        <div id="chart_div"></div>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="application/javascript">
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawCurveTypes);

function drawCurveTypes() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Agents');
      data.addColumn('number', 'Super Agents');
        data.addColumn('number', 'Merchnats');
        data.addColumn('number', 'Cells');
      data.addRows([
        [0, 0, 0,0,30],    [1, 10, 5,1,30],   [2, 23, 15,2,30],  [3, 17, 9,3,30],   [4, 18, 10,5,30],  [5, 9, 5,6,30],
        [6, 11, 3,7,30],   [7, 27, 19,8,30],  [8, 33, 25,9,30],  [9, 40, 32,10,30],  [10, 32, 24,11,30], [11, 35, 27,12,30]
      ]);

      var options = {
        hAxis: {
          title: 'Time'
        },
        vAxis: {
          title: 'Popularity'
        },
        series: {
          1: {curveType: 'function'}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
</script>
        <!--footer-->
        <?php
          include('footer.php');
        ?>
        <!--//footer-->
