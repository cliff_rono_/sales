<?php
require_once "../config/pik-functions.php";
class User{
    public static function getUserBalance($db,$user_id,$key){
        $sql = "SELECT account_balance FROM agents_pikash_accounts WHERE agent_id = ?";
        $stmt2 = $db->prepare($sql);
        $stmt2->execute([$user_id]);
        while($row2 = $stmt2->fetch(PDO::FETCH_OBJ)){
            $my_amount = $row2->account_balance;

        }

        $my_amount = my_decrypt($my_amount,$key);
        return $my_amount;
    }
    
    public static function getUserTransactionsCount($db,$user_id,$key){
     $query = "SELECT phone FROM user WHERE id = ?";
$stmt = $db->prepare($query);
if($stmt->execute([$user_id])){
while($row = $stmt->fetch(PDO::FETCH_OBJ)){

$phone = $row->phone;
}
}
        $sql = "SELECT * FROM transactions WHERE receiver_id = ? ORDER BY id DESC";
        $stmt2 = $db->prepare($sql);
        $stmt2->execute([$user_id]);
        return $stmt->rowCount();
    }
    
    
    
            public static function getAgentEarnings($db,$user_id,$key){
$query = "SELECT agent_number FROM agent WHERE id = ?";
$stmt = $db->prepare($query);
if($stmt->execute([$user_id])){
while($row = $stmt->fetch(PDO::FETCH_OBJ)){

$agent_number = $row->agent_number;
}
}
       
            
            
        $sql = "SELECT * FROM agent_earnings WHERE agent_number = ? ORDER BY id DESC";
        $stmt2 = $db->prepare($sql);
        $stmt2->execute([$agent_number]);
            $count = 1;
            $html = '';
        while($row2 = $stmt2->fetch(PDO::FETCH_OBJ)){
           $transaction_id = $row2->transaction_id;
            $ratio = $row2->ratio;
            $amount = $row2->amount;
            $date = $row2->date;
            $income = $row2->income;
            $fee = $row2->fee;

            $html .= "<tr>
            <td>$count</td>
            <td>$transaction_id</td>
            <td>$amount</td>
            <td>$fee</td>
            <td>$ratio%</td>
            <td>$income</td>
            <td>$date</td>

            </tr>";
            $count++;
           }

      return $html;  
    }
    
    
    
        public static function getAgentTransactions($db,$user_id,$key){
$query = "SELECT agent_number FROM agent WHERE id = ?";
$stmt = $db->prepare($query);
if($stmt->execute([$user_id])){
while($row = $stmt->fetch(PDO::FETCH_OBJ)){

$agent_number = $row->agent_number;
}
}
       
            
            
        $sql = "SELECT * FROM agent_transactions WHERE agent_number = ? ORDER BY id DESC";
        $stmt2 = $db->prepare($sql);
        $stmt2->execute([$agent_number]);
            $count = 1;
            $html = '';
        while($row2 = $stmt2->fetch(PDO::FETCH_OBJ)){
           $transaction_id = $row2->transaction_id;
            $transaction_type = $row2->type;
            $amount = $row2->amount;
            $date = $row2->date;
            $balance = $row2->balance_after;

            $html .= "<tr>
            <td>$count</td>
            <td>$transaction_id</td>
            <td>$transaction_type</td>
            <td>$amount</td>
            <td>$balance</td>
            <td>$date</td>

            </tr>";
            $count++;
           }

      return $html;  
    }
    
    public static function getMerchants($connection){
        $html = '';
        $query = "SELECT * FROM merchants";
        $stmt = $connection->prepare($query);
        $stmt->execute();
        while($row = $stmt->fetch(PDO::FETCH_OBJ)){
            $name = $row->name;
            $code = $row->code;
            $html .= '<option value="'.$code.'">'.$name.' - '.$code.'</option>';
        }
        return $html;
    }
}