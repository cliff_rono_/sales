

    <?php
      include('header.php');
  ?>


        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
<?php
$query = "SELECT * FROM user WHERE id = ?";
$stmt = $db->prepare($query);
$stmt->execute([$user_id]);
while($row = $stmt->fetch(PDO::FETCH_OBJ)){
    $first_name = $row->fname;
    $last_name = $row->lname;
    $email = $row->email;
    $phone = $row->phone;
}
?>
                <div class="elements  row">
                    <div class="col-md-4 profile widget-shadow">
                        <h4 class="title3">Profile</h4>
                        <div class="profile-top">
                            <img src="../assets/dashboard/user_profiles/profile.png" width="100" height="100" alt="">
                            <h4>
                                Pikash
                            </h4>
                            <h5><?php echo $first_name." ".$last_name;?></h5>
                        </div>
                        <div class="profile-text">
                            <div class="profile-row">
                                <div class="profile-left">
                                    <i class="fa fa-envelope profile-icon"></i>
                                </div>
                                <div class="profile-right">
                                    <h4>
                                        <?php echo $email; ?> </h4>
                                    <p>Email</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="profile-row row-middle">
                                <div class="profile-left">
                                    <i class="fa fa-mobile profile-icon"></i>
                                </div>
                                <div class="profile-right">
                                    <h4>
                                        <?php echo $phone; ?>
                                    </h4>
                                    <p>Contact Number</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="profile-row">
                                <div class="profile-left">
                                    <i class="fa fa-user profile-icon"></i>
                                </div>
                                <div class="profile-right">
                                    <h4 id="details">
                                        <?php echo $first_name." ".$last_name; ?>
                                    </h4>
                                    <p>Full Name</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <div class="profile-btm" style="padding:0px;">
                            <a id="success" href="#mymodal1" data-toggle="modal" data-target="#mymodal1" class="btn btn-primary btn-block" style="border-radius:0px;padding:20px 10px;">Change My Password</a>
                        </div>
                    </div>
                    <input type="hidden" name="" id="user" value="<?php echo $user_id; ?>">

					<div class="clearfix"> </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="mymodal1">
            <div class="modal-dialog">
                <div class="modal-content" style="background:#F1F1F1;border-radius:0px;">
                    <div class="modal-header" style="border:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="close" style="color:#000;">x</button>
                        <h2 class="text-center" style="color:#000;">Change Password</h2>
                    </div>
                    <form data-toggle="validator" action="" method="post" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="modal-body" style="border:none;">
                                <div class="form-group has-feedback">
                                    <input type="password" name="old_pass" class="form-control" id="old_pass" placeholder="Old Password" data-error="Sorry, this field is required" required>
                                    <span class="error"><?php if(isset($old_pass_err)){echo $pass_err;}?> </span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" name="new_pass" class="form-control" id="pass1" placeholder="New Password" data-error="Sorry, this field is required" required>
                                    <span class="error"><?php if(isset($new_pass_err)){echo $pass_err;}?> </span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" name="new_pass2" class="form-control" id="pass2" placeholder="Confirm New Password" data-error="Sorry, this field is required" required>
                                    <span class="error" id="passErr" style="color:red;display:none;font-weight:bold;font-size:18px;"></span>
                                    <span class="success" id="passsuccess" style="color:orangered;display:none;font-weight:bold;font-size:18px;"></span>
                                </div>
                            </div>
                            <div class="modal-footer" style="border:none;">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="form-group">
                                            <input type="submit" name="update_pass" value="Change Password" class="btn btn-success btn-block" style="padding:15px 10px;border-radius:0px;" id="conf_pass">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!--footer-->
        <?php
          include('footer.php');
      ?>
        <!--//footer-->
