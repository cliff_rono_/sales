

  <?php
    include ('header.php');
  ?>


        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                
               <br>
                <div class="tables">
                    <div class="panel-body widget-shadow">
                        <div id="chart_div" style="height:400px;width:100%;"></div>
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="application/javascript">
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawCurveTypes);
 
function drawCurveTypes() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Agents');
      data.addColumn('number', 'Super Agents');
        data.addColumn('number', 'Merchnats');
        data.addColumn('number', 'Cells');
      data.addRows([
        [0, 0, 0,0,30],    [1, 10, 5,1,30],   [2, 23, 15,2,30],  [3, 17, 9,3,30],   [4, 18, 10,5,30],  [5, 9, 5,6,30],
        [6, 11, 3,7,30],   [7, 27, 19,8,30],  [8, 33, 25,9,30],  [9, 40, 32,10,30],  [10, 32, 24,11,30], [11, 35, 27,12,30],
        [12, 30, 22,13,30], [13, 40, 32,14,30], [14, 42, 34,15,30], [15, 47, 39,16,30], [16, 44, 36,17,30], [17, 48, 40,18,30],
        [18, 52, 44,19,30], [19, 54, 46,20,30], [20, 42, 34,21,30], [21, 55, 47,22,30], [22, 56, 48,23,30], [23, 57, 49,24,30],
        [24, 60, 52,25,30], [25, 50, 42,26,30], [26, 52, 44,27,30], [27, 51, 43,28,30], [28, 49, 41,29,30], [29, 53, 45,30,30],
        [30, 55, 47,31,30]
      ]);

      var options = {
        hAxis: {
          title: 'Time'
        },
        vAxis: {
          title: 'Number'
        },
        series: {
          1: {curveType: 'function'}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
</script>
        <!--footer-->
        <?php
          include('footer.php');
        ?>
        <!--//footer-->
