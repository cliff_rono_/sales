



    <?php
      include('header.php');
    ?>

        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                <div class="forms validation">
                    <div class="row">
                 <form data-toggle="validator" action="../form-processing/withdrawals.php" method="post">
                                
                                <br>
                                <br>
                                <div class="col-md-12 validation-grids validation-grids-left" style="width:100%;">
                                    <div class="widget-shadow" data-example-id="basic-forms">
                                        <div class="form-title">
                                            <h4>Registration Form</h4>
                                        </div>
                                        <div class="form-body" style="">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group has-feedback">
                                                <input type="text" name="agent_name" class="form-control" id="" placeholder="Agent Name" data-error="Sorry, this field is required" required >
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors">Agent Name</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group has-feedback">
                                                <input type="text" name="registration_date" class="form-control" id="" placeholder="Registration Date" data-error="Sorry, this field is required" required >
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors">Agent Registration Date</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group has-feedback">
                                                <input type="text" name="designated_phone_number" class="form-control" id="" placeholder="Mobile Number" data-error="Sorry, this field is required" required >
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors">Outlet's Designated Mobile Number</span>
                                                </div>
                                            </div>
                                             <div class="col-sm-8">
                                                <div class="form-group has-feedback">
                                                <input type="checkbox" name="super_agent" id="">Super Agent
                                                <input type="checkbox" name="agent" id="">Agent
                                                <input type="checkbox" name="merchant" id="">Merchant
                                                <input type="checkbox" name="cell" id="">Cell
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group has-feedback">
                                            <input type="text" name="preferred_username" class="form-control" id="" placeholder="Preferred Username" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Preferred Pikash Username</span>
                                        </div>
                                            </div>
                                        </div>
<!-- PROPRIETOR OR OWNER'S CONTACT-->
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="proprietor_location" class="form-control" id="" placeholder="Location" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">City/Town/Market</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-6">
                                        <select name="poraeh_region" class="form-control selectpicker" data-live-search="true" required>
                                            <option value="">Select Poraeh Region</option>
                                            <?php $html = '';
        $query = "SELECT code,province,name FROM counties ORDER BY code ASC";
        $stmt = mysqli_query($connection,$query);
        while($row = mysqli_fetch_assoc($stmt)){
            $name = $row['name'];
            $code = $row['code'];
            $province = $row['province'];
           echo '<option value="'.$code.'">'.$code.' - '.$name.' - '.$province.'</option>';
        } ?>
                                        </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="operator_building" class="form-control" id="" placeholder="Building" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Operator Location - Building</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="operator_road" class="form-control" id="" placeholder="Road" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Operator Location - Road</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="operator_street" class="form-control" id="" placeholder="Street" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Operator Location - Street</span>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="nearest_landmark" class="form-control" id="" placeholder="Building" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Nearest Landmark</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="landmark_postal_address" class="form-control" id="" placeholder="Postal Address" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Postal Address of nearest landmark</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="landmark_postal_code" class="form-control" id="" placeholder="Postal Code" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Postal Code For Landmark</span>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="proprietor_phone" class="form-control" id="" placeholder="Phone Number" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Contact Phone Number</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                             <div class="form-group has-feedback">
                                            <input type="text" name="proprietor_email" class="form-control" id="" placeholder="Email Address" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Contact Email</span>
                                        </div>
                                         </div>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_surname[]" class="form-control" id="" placeholder="Surname" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">1. Surname</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_middle_name[]" class="form-control" id="" placeholder="Middle Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">1. Middle Name</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_first_name[]" class="form-control" id="" placeholder="First Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">1. First Name</span>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                          <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_ID[]" class="form-control" id="" placeholder="Attendant 1 ID/Passport" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 1 ID/Passport</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_phone[]" class="form-control" id="" placeholder="Attendant 1 Phone" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 1 Phone</span>
                                        </div>
                                        </div> 
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_surname[]" class="form-control" id="" placeholder="Surname" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">2. Surname</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_middle_name[]" class="form-control" id="" placeholder="Middle Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">2. Middle Name</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_first_name[]" class="form-control" id="" placeholder="First Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">2. First Name</span>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                          <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_ID[]" class="form-control" id="" placeholder="Attendant 2 ID/Passport" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 2 ID/Passport</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_phone[]" class="form-control" id="" placeholder="Attendant 2 Phone" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 2 Phone</span>
                                        </div>
                                        </div> 
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                            <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_surname[]" class="form-control" id="" placeholder="Surname" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">3. Surname</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_middle_name[]" class="form-control" id="" placeholder="Middle Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">3. Middle Name</span>
                                        </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_first_name[]" class="form-control" id="" placeholder="First Name" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">3. First Name</span>
                                        </div>
                                        </div>
                                        <div class="row" style="padding:0px;">
                                          <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_ID[]" class="form-control" id="" placeholder="Attendant 3 ID/Passport" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 3 ID/Passport</span>
                                        </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group has-feedback">
                                            <input type="text" name="attendant_phone[]" class="form-control" id="" placeholder="Attendant 3 Phone" data-error="Sorry, this field is required" required >
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors">Attendant 3 Phone</span>
                                        </div>
                                        </div> 
                                        </div>
                                        </div>
                                        </div>
                                        
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" name="add_site">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>

        <!--footer-->

        <?php
          include('footer.php');
        ?>

        <!--//footer-->
