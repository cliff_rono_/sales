

  <?php
    include ('header.php');
  ?>


        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
          
                <div class="tables">
                    <div class="panel-body widget-shadow">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Names</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Sales People</th>
                                    <th>Agents</th>
                                    <th>Super Agents</th>
                                    <th>Merchants</th>
                                    <th>Cells</th>
<!--                                    <th>More</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Cliff Rono</td>
                                    <td>0712574847</td>
                                    <td>someguy@example.com</td>
                                    <td><a href="team-leaders.php">58 people</a></td>
                                    <td><a href="overall-reports-agents.php">10</a></td>
                                    <td><a href="overall-reports-super-agents.php">22</a></td>
                                    <td><a href="overall-reports-merchants.php">5</a></td>
                                    <td><a href="overall-reports-cells.php">27</a></td>
<!--                                    <td><a href="district-leader.php">View</a></td>-->
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--footer-->
        <?php
          include('footer.php');
        ?>
        <!--//footer-->
