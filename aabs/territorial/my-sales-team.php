

  <?php
    include ('header.php');
  ?>


        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
          
                <div class="tables">
                    <div class="panel-body widget-shadow">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                   <th>No</th>
                                    <th>Names</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>District</th>
                                    <th>Sales People</th>
                                    <th>Date Added</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>Cliff Rono</td>
                                  <td>0712345678</td>
                                  <td>someone@example.com</td>
                                  <td>Kericho</td>
                                  <td><a href="district-team-leaders.php">52 people</a></td>
                                  <td>12-12-2016</td>  
                                  
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--footer-->
        <?php
          include('footer.php');
        ?>
        <!--//footer-->
