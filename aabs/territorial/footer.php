

<div class="footer">
<p>&copy; <?php echo date("Y"); ?> Pikash. All Rights Reserved.</p>
</div>

</div>
<!-- Classie -->
<script src="../assets/dashboard/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../assets/dashboard/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../assets/dashboard/vendors/jszip/dist/jszip.min.js"></script>
<script src="../assets/dashboard/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../assets/dashboard/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../assets/dashboard/js/classie.js"></script>
<script src="../assets/dashboard/js/custom.min.js"></script>
<script src="../js/pikash-forms.js"></script>

<script>
    $(document).ready(function() {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
            
        });

</script>
<script type="application/javascript">
$(document).ready(function() {
    $('.table').dataTable();
});
var menuLeft = document.getElementById('cbp-spmenu-s1'),
    showLeftPush = document.getElementById('showLeftPush'),
    body = document.body;

showLeftPush.onclick = function() {
    classie.toggle(this, 'active');
    classie.toggle(body, 'cbp-spmenu-push-toright');
    classie.toggle(menuLeft, 'cbp-spmenu-open');
    disableOther('showLeftPush');
};

function disableOther(button) {
    if (button !== 'showLeftPush') {
        classie.toggle(showLeftPush, 'disabled');
    }
}

</script>
<!--scrolling js-->
<script src="../assets/dashboard/js/jquery.nicescroll.js"></script>
<script src="../assets/dashboard/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="../assets/dashboard/js/bootstrap.js">


</script>
</body>

</html>
